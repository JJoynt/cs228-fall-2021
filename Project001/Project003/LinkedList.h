// LinkedList.h
#include <iostream>
#ifndef __LINKEDLIST_H__
#define __LINKEDLIST_H__
using namespace std;

struct Customer {
    int ID;
    string adr;

};

struct Node
{
    Customer data;
    Node* next;
};

class LinkedList
{
public:
  LinkedList();         // Default constructor, the list is empty to start
  ~LinkedList();        // Default destructor, must delete all nodes

  void InsertFront(Customer* value); // Add a node containing "value" to the front
  
  Customer* GetNode(unsigned int index); // Return value at position "index"
  int Search(int value); // Return the index of the node containing "value"
  
  Customer* DeleteNode(unsigned int index); // Delete the node at position "index", return the value that was there

  // This function reverses the order of all nodes so the first node is now the
  // last and the last is now the first (and all between). So a list containing 
  // (4, 0, 2, 11, 29) would be (29, 11, 2, 0, 4) after Reverse() is called.

  int GetLength();        // Return the number of nodes in the list

private:

    Node* head;
  // Add class members here

};


#endif

