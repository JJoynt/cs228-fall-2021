/*
Name: John Joynt
Date: 07 Nov 2021
Task: Programming Assignment 3 - Midterm
*/

#include <stdlib.h>
#include "LinkedList.h"

//Default constructor, the list is empty to start
LinkedList::LinkedList() {
   
}

//Default destructor, must delete all nodes
LinkedList::~LinkedList() {
    while (GetLength() != 0) {
        DeleteNode(GetLength()-1);
    }
}

//Add a node containing "value" to the front
void LinkedList::InsertFront(Customer* value) {
    Node newNode;
    newNode.data = *value;
    newNode.next = head;
    head = &newNode;
}

//Return value at position "index"
Customer* LinkedList::GetNode(unsigned int index) {
    Node* temp = head;
    
    try {
        for (int i = 0; i < index; i++) {
            temp = temp->next;
        }
    }
    catch (...) {
        return nullptr;
    }
        
    return &temp->data;
}

//Return the index of the node containing "value"
int LinkedList::Search(int value) {
    Node* temp = head;

    int j = 0;
    try {
        for (; temp->data.ID != value; j++) {
            temp = temp->next;

            //if (temp == nullptr) {
            //    return -1;
            //}
        }
    }
    catch (...) {
        return -1;
    }
    return j;
}


//Ability to delete nodes
Customer* LinkedList::DeleteNode(unsigned int index) {
    Node* temp = head;

    for (int k = 0; k < index - 1; k++) {
        temp = temp->next;
    }

    Node* newNext = temp->next->next;
    Customer* retVal = &temp->next->data;
    delete temp->next;
    temp->next = newNext;

    return retVal;
}

//Return the number of nodes in the list
int LinkedList::GetLength() {
    Node* temp = head;

    int l = 0;
    for (; temp->next == nullptr; l++) {
        temp = temp->next;
    }

    return l + 1;
}