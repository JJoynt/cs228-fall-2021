// main.cpp

#include <iostream>
#include "../Airport.h"
using namespace std;

int main() {
    
    char x[128];
    
    cout << "Starting the first simulation." << endl;

    // (1) Landing time, (2) takeoff time, (3) arrival probability, 
    // (4) take off probability, (5) max time, and (6) simulation length
    Airport(5, 2, 0.1, 0.2, 45, 120);
    
    cout << "First simulation finished." << endl << endl;

    cout << "Starting the second simulation." << endl;

    // (1) Landing time, (2) takeoff time, (3) arrival probability, 
    // (4) take off probability, (5) max time, and (6) simulation length
    Airport(15, 10, 0.9, 0.9, 30, 1440);

    cout << "Second simulation finished." << endl;
    
    return 0;
}