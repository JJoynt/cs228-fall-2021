/*
Name: John Joynt
Date: 12 December 2021
Task: CS228 Final Project
*/

#include "Airport.h"
#include <queue>
#include <cstdlib>
#include <iostream>
using namespace std;

// Default constructor
void Airport( 
        int landingTime, // Time segments needed for one plane to land
        int takeoffTime, // Time segs. needed for one plane to take off
        double arrivalProb, // Probability that a plane will arrive in
        // any given segment to the landing queue
        double takeoffProb, // Probability that a plane will arrive in
        // any given segment to the takeoff queue
        int maxTime, // Maximum number of time segments that a plane
        // can stay in the landing queue
        int simulationLength// Total number of time segs. to simulate
        ) {
    queue<int> takeoffQ, landingQ;
    int landingWaits = 0, takeoffWaits = 0, timeFree = 0, crashes = 0;
    int numLandings = 0, numTakeoffs = 0;

    // Uses computer's clock for RNG 
    srand(time(NULL));

    // Run as many times as there are time segments
    for (int curTime = 0; curTime < simulationLength; curTime++) {

        if (((double) rand() / (double) RAND_MAX) < arrivalProb) {

            landingQ.push(curTime); // Determines if new plane has been added
                                    // to the landing queue
        }

        if (((double) rand() / (double) RAND_MAX) < takeoffProb) {

            takeoffQ.push(curTime); // Determines if new plane has been added
                                    // to takeoff queue
        }

        // Continue iteration if time free is less than or equal to current time
        if (timeFree <= curTime) {

            if (!landingQ.empty()) {

                // First plane in landing queue after max time segments crash
                if ((curTime - landingQ.front()) > maxTime) {
                    crashes++;
                    landingQ.pop();
                } else { // First plane in landing queue lands
                    landingWaits += (curTime - landingQ.front());
                    landingQ.pop();
                    timeFree += landingTime;
                    numLandings++;
                }
            }
            
            else if (!takeoffQ.empty()) { // Planes waiting to takeoff, takeoff

                takeoffWaits += (curTime - takeoffQ.front());
                takeoffQ.pop();
                timeFree += takeoffTime;
                numTakeoffs++;
            }
        }
    }

    // Remaining planes after time segments completes crash
    while (!landingQ.empty() && (simulationLength - landingQ.front()) > maxTime) {
        crashes++;
        landingQ.pop();
    }

    cout << "Number of crashes: " << crashes << "\n";
    cout << "Number of takeoffs: " << numTakeoffs << " (avg delay " <<
            ((double) takeoffWaits) / ((double) numTakeoffs) << ")\n";
    cout << "Number of landings: " << numLandings << " (avg delay " <<
            ((double) landingWaits) / ((double) numLandings) << ")\n";
    cout << "Number of planes in takeoff queue: " << takeoffQ.size() << "\n";
    cout << "Number of planes in landing queue: " << landingQ.size() << "\n";
    cout << "Last plane's fuel level: " << trackFuel(100) << "%" << "\n"; // All planes start with max fuel
}

int trackFuel(int currentFuel) {
    int newFuel = rand() % currentFuel;
  
    return newFuel;
}