// Airport.h
#include<iostream>
#include <iomanip>
#include <math.h>
#include <queue>
using namespace std;

#ifndef __AIRPORT_H__
#define __AIRPORT_H__

// Declaration of fuel function
int trackFuel(int newFuel);

// To check if the plane going to queue
static bool
ifPlaneComingIntorunwayQueue(int avgInterval)
{
    double drand = rand() / (double)RAND_MAX;
    if (drand < (1.0 / avgInterval))
        return true;
    else
        return false;

}

// to check if plane crashed
static bool
if_PlaneCrashed(int in, int out, int interval) {
    if (out - in > interval) {
        return true;
    }
    else {
        return false;
    }

}

void Airport(
  int landingTime,    // Time segments needed for one plane to land
  int takeoffTime,    // Time segs. needed for one plane to take off
  double arrivalProb, // Probability that a plane will arrive in
                      // any given segment to the landing queue
  double takeoffProb, // Probability that a plane will arrive in
                      // any given segment to the takeoff queue
  int maxTime,        // Maximum number of time segments that a plane
                      // can stay in the landing queue
  int simulationLength// Total number of time segs. to simulate
);

#endif